package org.xiomi.notifier.activities.fragment

import android.os.Bundle
import androidx.preference.EditTextPreference
import androidx.preference.EditTextPreferenceDialogFragmentCompat
import androidx.preference.PreferenceFragmentCompat

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        val context = preferenceManager.context
        val screen = preferenceManager.createPreferenceScreen(context)

        val checkinTarget = EditTextPreference(context)
        checkinTarget.key = "checkin_url"
        checkinTarget.title = "Check In FCM URL"

        screen.addPreference(checkinTarget)

        preferenceScreen = screen

    }
}