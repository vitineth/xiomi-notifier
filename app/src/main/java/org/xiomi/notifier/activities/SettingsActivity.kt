package org.xiomi.notifier.activities

import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.add
import androidx.fragment.app.commit
import org.xiomi.notifier.R
import org.xiomi.notifier.activities.fragment.SettingsFragment

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.setting_activity_container)

//        val config: Configuration = resources.configuration
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            add<SettingsFragment>(R.id.fragmentContainerView)
        }
    }

}