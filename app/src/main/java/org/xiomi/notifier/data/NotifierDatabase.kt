package org.xiomi.notifier.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import org.xiomi.notifier.data.converters.Converters
import org.xiomi.notifier.data.daos.EventDAO
import org.xiomi.notifier.data.entities.Event

@Database(entities = [Event::class], version = 1)
@TypeConverters(Converters::class)
abstract class NotifierDatabase : RoomDatabase() {

    abstract fun eventDao(): EventDAO

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: NotifierDatabase? = null

        fun getDatabase(context: Context): NotifierDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    NotifierDatabase::class.java,
                    "notifier-db"
                ).build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}