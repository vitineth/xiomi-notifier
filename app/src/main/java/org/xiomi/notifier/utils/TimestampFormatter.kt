package org.xiomi.notifier.utils

import android.os.Handler
import android.os.Looper
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.TextUnit
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import kotlin.math.floor
import kotlin.math.min

const val MINUTE_MULTIPLIER = 60F
const val HOUR_MULTIPLIER = MINUTE_MULTIPLIER * 60
const val DAY_MULTIPLIER = HOUR_MULTIPLIER * 24

fun formatTime(time: ZonedDateTime): String {
    val rawSec = ChronoUnit.SECONDS.between(time, ZonedDateTime.now(ZoneOffset.UTC));

    val days = floor(rawSec / DAY_MULTIPLIER).toInt()
    if (days > 7) return time.toString()
    if (days > 1) return "$days days ago"

    val hours = floor(rawSec / HOUR_MULTIPLIER).toInt()
    val minutes = floor((rawSec - (hours * HOUR_MULTIPLIER)) / MINUTE_MULTIPLIER).toInt()
    val seconds = floor(rawSec - (hours * HOUR_MULTIPLIER) - (minutes * MINUTE_MULTIPLIER)).toInt()

    val output = mutableListOf<String>();
    if (hours != 0) output.add("$hours hours")
    if (minutes != 0) output.add("$minutes minutes")
    if (seconds != 0) output.add("$seconds seconds")

    if (hours == 0 && minutes == 0 && seconds == 0) return "now";
    return output.joinToString() + " ago"
}

@Composable
fun TimeSince(
    time: ZonedDateTime,
    modifier: Modifier = Modifier,
    color: Color = Color.Unspecified,
    fontSize: TextUnit = TextUnit.Unspecified,
    fontStyle: FontStyle? = null,
    fontWeight: FontWeight? = null,
    fontFamily: FontFamily? = null,
    letterSpacing: TextUnit = TextUnit.Unspecified,
    textDecoration: TextDecoration? = null,
    textAlign: TextAlign? = null,
    lineHeight: TextUnit = TextUnit.Unspecified,
    overflow: TextOverflow = TextOverflow.Clip,
    softWrap: Boolean = true,
    maxLines: Int = Int.MAX_VALUE,
    onTextLayout: (TextLayoutResult) -> Unit = {},
    style: TextStyle = LocalTextStyle.current
) {
    var value by remember { mutableStateOf(formatTime(time)) }

    DisposableEffect(Unit) {
        val handler = Handler(Looper.getMainLooper())

        val runnable = {
            value = formatTime(time)
        }

        handler.postDelayed(runnable, 30_000)

        onDispose {
            handler.removeCallbacks(runnable)
        }
    }

    return Text(
        value,
        modifier,
        color,
        fontSize,
        fontStyle,
        fontWeight,
        fontFamily,
        letterSpacing,
        textDecoration,
        textAlign,
        lineHeight,
        overflow,
        softWrap,
        maxLines,
        onTextLayout,
        style
    )
}