package org.xiomi.notifier.firebase

import android.app.ActivityManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.xiomi.notifier.MainActivity
import org.xiomi.notifier.R
import org.xiomi.notifier.activities.EventActivity
import org.xiomi.notifier.api.XiomiAPI
import org.xiomi.notifier.data.entities.Event
import org.xiomi.notifier.data.EventState
import org.xiomi.notifier.data.NotifierDatabase
import java.time.Instant
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.util.*

open class XiomiFirebaseMessagingService : FirebaseMessagingService() {

    companion object CoreData {
        private val TAG = XiomiFirebaseMessagingService::class.simpleName
        var identifier: String? = null

        fun configure(context: Context) {
            Log.d(TAG, "Sending request")
            FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                val token = task.result
                if (token == null) {
                    Log.w(TAG, "Initial token was not valid")
                    return@OnCompleteListener
                }

                Log.d("Initial token", token)
                identifier = token
                XiomiAPI.sendRegistrationKey(context, token)
            })
        }
    }

    private var lastNotificationID = 0

    /**
     * Called if the FCM registration token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the
     * FCM registration token is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // FCM registration token to your app server.
        XiomiAPI.sendRegistrationKey(this, token)
    }

    private fun verifyPayload(payload: Map<String, String>): Boolean {
        val requiredKeys = listOf(
            "status", "module", "topic",
            "summary", "detail", "timestamp"
        )

        if (!requiredKeys.stream().allMatch {
                val result = payload.containsKey(it)
                if (!result) Log.w(TAG, "Message was missing $it")
                return@allMatch result
            }) return false

        if (payload.containsKey("correlation_id")) {
            try {
                UUID.fromString(payload["correlation_id"])
            } catch (_: IllegalArgumentException) {
                Log.w(TAG, "correlation_id was not a uuid: ${payload["correlation_id"]}")
                return false
            }
        }

        if (payload.containsKey("uid")) {
            try {
                UUID.fromString(payload["uid"])
            } catch (_: IllegalArgumentException) {
                Log.w(TAG, "uid was not a uuid: ${payload["uid"]}")
                return false
            }
        }

        try {
            EventState.valueOf(payload["status"]!!)
        } catch (e: IllegalArgumentException) {
            Log.w(TAG, "status was not a status: ${payload["status"]}")
            return false
        } catch (e: NullPointerException) {
            Log.w(TAG, "status was not a status: ${payload["status"]}")
            return false
        }

        try {
            payload["timestamp"]!!.toLong(10)
        } catch (e: NumberFormatException) {
            Log.w(TAG, "timestamp was not a long: ${payload["timestamp"]}")
            return false
        } catch (e: NullPointerException) {
            Log.w(TAG, "timestamp was not a long: ${payload["timestamp"]}")
            return false
        }

        return true
    }

    override fun onMessageReceived(p0: RemoteMessage) {
        Log.d(TAG, "Message form ${p0.from}")

        var eventID: String? = null
        if (p0.data.isNotEmpty()) {
            if (!verifyPayload(p0.data)) {
                return
            }

            val excluded = listOf(
                "correlation_id", "status", "module", "topic",
                "summary", "detail", "timestamp", "properties", "timestamp",
                "uid"
            )
            val properties = p0.data.filterKeys { !excluded.contains(it) }

            Log.d("TIMESTAMP", p0.data["timestamp"] ?: "not there");
            val event = Event(
                UUID.fromString(p0.data["uid"]) ?: UUID.randomUUID(),
                p0.data["correlation_id"]?.let { UUID.fromString(it) },
                EventState.valueOf(p0.data["status"]!!),
                p0.data["module"]!!,
                p0.data["topic"]!!,
                p0.data["summary"]!!,
                p0.data["detail"],
                properties,
                p0.data["timestamp"]?.toLong()
                    ?.let { ZonedDateTime.ofInstant(Instant.ofEpochSecond(it), ZoneOffset.UTC) }
                    ?: ZonedDateTime.now(ZoneOffset.UTC)
            )

            eventID = event.uid.toString()
            NotifierDatabase.getDatabase(this).let {
                it.eventDao().insertAll(event)
                Log.d(TAG, "Event ${event.uid} inserted successfully from firebase ${p0.messageId}")

            }
        } else {
            Log.d(TAG, "Message has no additional info")
        }

        val notification = p0.notification
        if (isInForeground() && notification !== null) {
            val intent =
                Intent(
                    this@XiomiFirebaseMessagingService,
                    eventID?.let { EventActivity::class.java } ?: MainActivity::class.java
                ).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                }

            eventID?.let {
                intent.putExtra("event_id", eventID)
            }

            val pending = PendingIntent.getActivity(
                this@XiomiFirebaseMessagingService,
                0,
                intent,
                PendingIntent.FLAG_IMMUTABLE
            )

            val channelId = getString(R.string.default_notification_channel_id)
            val generated = NotificationCompat.Builder(this, channelId)
                .setContentTitle(notification.title)
                .setContentText(notification.body)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pending)
                .setAutoCancel(true)
                .build()

            with(NotificationManagerCompat.from(this@XiomiFirebaseMessagingService)) {
                notify(lastNotificationID++, generated)
            }
        }
    }

    private fun isInForeground(): Boolean {
        val systemService: ActivityManager =
            applicationContext.getSystemService(ACTIVITY_SERVICE) as ActivityManager
        val runningAppProcesses = systemService.runningAppProcesses
        val appID = applicationContext.packageName

        return runningAppProcesses.find { it.processName == appID }?.let {
            return@let it.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND
        } ?: false
    }

}